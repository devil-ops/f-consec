#
# Common Log Format Output.  This is useful for splunk parsing
#
class ClfOutput(object):
    def init(self):
        pass

    def format_data(self, data):
        """
        Log that data
        """
        line = ""
        for k, v in data.items():
            if str(v).find(' ') != -1:
                line += "%s='%s' " % (k, v)
            else:
                line += "%s=%s " % (k, v)
        return(line.strip())
