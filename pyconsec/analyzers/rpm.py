import requests
import requests_cache
from datetime import timedelta
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry


class RpmAnalyzer(object):
    """
    Class to analyze rpms
    """
    def __init__(self, cache_expiry_hours=24):
        requests_cache.install_cache('rpm_cache',
                                     expire_after=timedelta(
                                         cache_expiry_hours))
        self.s = requests.Session()
        retry_count = 5
        retry = Retry(total=retry_count, read=retry_count, connect=retry_count,
                      backoff_factor=3)
        self.s.mount('https://access.redhat.com',
                     HTTPAdapter(max_retries=retry))
        self.api_host = 'https://access.redhat.com/labs/securitydataapi'

    def get_data(self, query):
        full_query = self.api_host + query
        r = self.s.get(full_query)

        if r.status_code != 200:
            print('ERROR: Invalid request; returned {} for the following '
                  'query:\n{}'.format(r.status_code, full_query))
            return []

        return r.json()

    def query_package(self, name, version, release=None, cvss_score=None):
        params = 'package=%s-%s-%s' % (name, version, release)
        if cvss_score:
            params += '&cvss_score=%s' % cvss_score
        data = self.get_data('/cve.json?%s' % (params))
        return data

    def query_packages(self, packages, cvss_score=None):
        params = 'package=%s' % ",".join(packages)
        if cvss_score:
            params += '&cvss_score=%s' % cvss_score
        return self.get_data('/cve.json?%s' % (params))

    def get_cves_for_package(self, package_name):
        cves = []
        cve_data = self.query_package(package_name)

        if not cve_data:
            cves = []

        for cve in cve_data:
            if package_name in cve['affected_packages']:
                cves.append(cve)
        return cves
