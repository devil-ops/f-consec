import requests
import requests_cache
from datetime import timedelta


class CveAnalyzer(object):
    """
    Get information about a CVE
    """

    def __init__(self, cve, cache_expiry_hours=24):
        requests_cache.install_cache('cve_cache',
                                     expire_after=timedelta(
                                         cache_expiry_hours))
        self.circl = requests.get(
            'https://cve.circl.lu/api/cve/%s' % cve).json()

    def get_cvss_score(self):
        try:
            return float(self.circl['cvss'])
        except:
            return 0
