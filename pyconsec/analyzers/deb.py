import requests
import requests_cache
from datetime import timedelta
import json
from pyconsec.analyzers.cve import CveAnalyzer


class DebAnalyzer(object):
    """
    Class to analyze rpms
    """
    def __init__(self, cache_expiry_hours=24):
        requests_cache.install_cache('deb_cache',
                                     expire_after=timedelta(
                                         cache_expiry_hours))
        usn_url = 'https://usn.ubuntu.com/usn-db/database.json'
        self.usn_info = json.loads(requests.get(usn_url).text)

    def get_data(self, query):
        full_query = self.api_host + query
        r = requests.get(full_query)

        if r.status_code != 200:
            print('ERROR: Invalid request; returned {} for the following '
                  'query:\n{}'.format(r.status_code, full_query))
            return None

        return r.json()

    def query_package(self, lsb_codename, name, version, cvss_score=None):
        from pydpkg import Dpkg

        cves = []
        for usn, usn_data in self.usn_info.items():
            affected_releases = usn_data['releases'].keys()

            # Don't worry about releases that aren't affected
            if lsb_codename not in affected_releases:
                continue

            # Skip packages we don't care about
            package_names = usn_data['releases'][lsb_codename]['binaries'].keys()
            if name not in package_names:
                continue

            ## SKip this junk
            if Dpkg.compare_versions(version, usn_data['releases'][lsb_codename]['binaries'][name]['version']) >= 0:
                continue

            for cve in usn_data['cves']:
                c = CveAnalyzer(cve)
                t_cvss_score = c.get_cvss_score()
                if t_cvss_score < cvss_score:
                    continue
                cves.append(c.circl)
        return cves

    def get_cves_for_package(self, package_name):
        cves = []
        cve_data = self.query_package(package_name)

        if not cve_data:
            cves = []

        for cve in cve_data:
            if package_name in cve['affected_packages']:
                cves.append(cve)
        return cves
