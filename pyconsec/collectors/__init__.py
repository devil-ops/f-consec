"""
Base collector for things like local, remotessh,e tc
"""
import json
import subprocess
from datetime import datetime


class base_collector(object):
    def __init__(self, remote_host=None, image_name=None):
        self.data = []
        self.remote_host = remote_host
        if image_name:
            self.collect_image_info(image_name)
        else:
            self.collect_container_info()

    def collect_image_info(self, image_name):
        self.container_ids = self.list_container_ids()
        # Get rpm info
        container_info = {
            'packages': {},
            'meta': {}
        }
        container_info['packages']['rpms'] = self.list_rpms(image_name=image_name)
        container_info['packages']['debs'] = self.list_debs(image_name=image_name)
        container_info['container_inspect'] = self.inspect_image(image_name)

        container_info['meta']['hostname'] = ''

        try:
            container_info['meta']['lsb_codename'] = bytes\
                .decode(self.get_command_output(
                    "docker run --rm lsb_release %s -s -c" % image_name,
                    shell=True)).strip()
        except:
            pass

        container_info['meta']['collected_at'] = datetime.now()\
            .isoformat()

        self.data.append(container_info)

    def collect_container_info(self):
        self.container_ids = self.list_container_ids()
        # Get rpm info
        for container_id in self.container_ids:
            container_info = {
                'packages': {},
                'meta': {}
            }
            container_info['packages']['rpms'] = self.list_rpms(container_id)
            container_info['packages']['debs'] = self.list_debs(container_id)

            container_info['container_inspect'] = self\
                .inspect_container(container_id)

            container_info['meta']['hostname'] = bytes.decode(self.get_command_output("hostname -f")).strip()

            try:
                container_info['meta']['lsb_codename'] = bytes\
                    .decode(self.get_command_output(
                        "docker exec %s lsb_release -s -c" % container_id,
                        shell=True)).strip()
            except:
                pass

            container_info['meta']['collected_at'] = datetime.now()\
                .isoformat()

            self.data.append(container_info)

    def print_json(self):
        print(json.dumps(self.data))

    def get_command_output(self, cmd, user='root'):

        if self.remote_host:
            parts = ["ssh -q -o Batchmode=yes -o StrictHostKeyChecking=no -o",
                     "ConnectTimeout=3 root@%s \"%s\"" % (self.remote_host,
                                                          cmd)]
            cmd = " ".join(parts)

        import sys
        sys.stderr.write("Command: %s\n" % repr(cmd))
        return subprocess.check_output(cmd, shell=True)

    def inspect_container(self, container_id):
        inspect_output = self.get_command_output(
            "docker inspect --format '{{ json . }}' %s" % container_id)
        return json.loads(bytes.decode(inspect_output))

    def inspect_image(self, image_name):
        inspect_output = self.get_command_output(
            "docker inspect --format '{{ json . }}' %s" % image_name)
        return json.loads(bytes.decode(inspect_output))

    def list_container_ids(self):
        container_ids = []
        for item in self.get_command_output('docker ps -q').splitlines():
            item = item.strip()
            container_id = bytes.decode(item.split()[0])
            container_ids.append("%s" % container_id)
        return container_ids

    def list_debs(self, container_id=None, image_name=None):
        if not container_id and not image_name:
            raise Exception("Need either a container_id or an image_id")
        if container_id and image_name:
            raise Exception("Only use a container_id or an image_id")

        if container_id:
            query = "docker exec %s dpkg-query -W -f='${Package} ${Version} ${Architecture}\n'" % container_id
        else:
            query = "docker run --rm --entrypoint dpkg-query %s -W -f='${Package} ${Version} ${Architecture}\n'" % image_name
        debs = []
        try:
            deb_output = self.get_command_output(query).splitlines()
        except:
            return []

        for item in deb_output:
            item = bytes.decode(item.strip())
            try:
                (name, version, arch) = item.split()
            except:
                continue
            debs.append(
                {
                    'name': name,
                    'version': version,
                    'arch': arch
                }
            )

        return debs

    def list_rpms(self, container_id=None, image_name=None):
        if not container_id and not image_name:
            raise Exception("Need either a container_id or an image_id")
        if container_id and image_name:
            raise Exception("Only use a container_id or an image_id")

        if container_id:
            query = "docker exec %s rpm -qa --queryformat '%%{NAME} %%{VERSION} %%{RELEASE} %%{ARCH}\n'" % (container_id)
        else:
            query = "docker run --rm --entrypoint rpm %s -qa --queryformat '%%{NAME} %%{VERSION} %%{RELEASE} %%{ARCH}\n'" % (image_name)

        rpms = []
        try:
            rpm_output = self.get_command_output(query).splitlines()
        except:
            return []

        for item in rpm_output:
            item = bytes.decode(item.strip())

            (name, version, release, arch) = item.split()
            rpms.append(
                {
                    'name': name,
                    'version': version,
                    'release': release,
                    'arch': arch
                }
            )

        return rpms
