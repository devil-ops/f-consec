import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


with open('requirements.txt') as requirements_file:
    install_requirements = requirements_file.read().splitlines()

setup(
    name="f-consec",
    version="0.0.8",
    author="Drew Stinnett",
    author_email="drew.stinnett@duke.edu",
    description=("Scan containers for CVEs"),
    license="MIT",
    keywords="container security scan cve",
    packages=find_packages(),
    install_requires=install_requirements,
    scripts=[
        'scripts/fconsec-scan-packagelist-cve',
        'scripts/fconsec-enumerate-local-docker',
        'scripts/fconsec-enumerate-remote-docker-ssh',
        'scripts/fconsec-enumerate-remote-docker',
        'scripts/fconsec-enumerate-local-image'
    ],
    long_description=read('README.md'),
)
