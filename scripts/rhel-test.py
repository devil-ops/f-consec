#!/usr/bin/env python3

## Sample command to get output
## docker run -ti centos:5 rpm -qa --queryformat "%{NAME} %{VERSION} %{RELEASE} %{ARCH}\n"
## Using this api for redhat
##   https://access.redhat.com/blogs/766093/posts/2998921
import sys
from pyconsec.analyzers.rpm import RpmAnalyzer


def main():
    analyzer = RpmAnalyzer()
    fh = open('example_data/centos6_docker.txt', 'r')
    for line in fh.readlines():
        line = line.strip()
        (name, version, release, arch) = line.split()
        package_name = "%s-%s-%s" % (name, version, release)
        cves = analyzer.query_package(package_name)
        if len(cves) == 0:
            continue
        print(package_name, cves)
    fh.close()
    return 0

if __name__ == "__main__":
    sys.exit(main())
